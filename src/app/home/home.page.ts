import { Expense } from "./expense.model";
import { Component } from "@angular/core";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  expenseReason: string;
  expenseAmount: number;
  expenseList: Array<Expense> = [];
  totalExpense: number = 0;
  ngOnInit() {
    console.log(this.expenseReason, " ", this.expenseAmount);
  }
  constructor(public alertController: AlertController) {}
  onClear() {
    this.expenseAmount = null;
    this.expenseReason = null;
  }

  onAddExpense() {
    if (!this.expenseReason || !this.expenseAmount) {
      this.showAlert();
    } else {
      console.log(this.expenseReason, " ", this.expenseAmount);
      this.totalExpense += this.expenseAmount;
      let newExpense = new Expense();
      newExpense.expenseReason = this.expenseReason;
      newExpense.expenseAmount = this.expenseAmount;
      this.expenseList.push(newExpense);
      this.onClear();
    }
  }
  async showAlert() {
    const alert = await this.alertController.create({
      header:'Invalid Inputs',
      message:'<b>Please enter valid reason and amount!</b>',
      buttons:['OK']
    });
    await alert.present();
  }
}
